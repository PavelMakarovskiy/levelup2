package ru.levelUp2.universities.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class FacultyJdbcStorage {
    private JdbcService jdbcService;

    public FacultyJdbcStorage() {
        this.jdbcService = new JdbcService();
    }

    public void findBySql(String sql) {
        try (Connection connection = jdbcService.openConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String name = rs.getString(2);
                int uId = rs.getInt(3);

                System.out.println(id + " " + name + " " + uId);
            }
        } catch (SQLException exc) {
            System.out.println("Can not open connection");
            System.out.println(exc.getMessage());
        }
    }
}
