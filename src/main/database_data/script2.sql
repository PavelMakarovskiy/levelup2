create table university
(
  id              serial      not null
    constraint university_pkey
    primary key,
  name            varchar     not null
    constraint university_name_key
    unique,
  short_name      varchar(20) not null
    constraint university_short_name_key
    unique,
  foundation_year integer     not null
);

alter table university owner to level;

create table faculty
(
  faculty_id    serial  not null
    constraint faculty_pkey
    primary key,
  name          varchar not null,
  university_id integer not null
    constraint faculty_university_id_fkey
    references university
);

alter table faculty owner to level;

create table subject
(
  id             serial  not null
    constraint subject_pkey
    primary key,
  subject        varchar not null,
  count_of_hours integer not null,
  faculty_id     integer not null
);

alter table subject owner to level;

create table subject_info
(
  subject_id  serial not null
    constraint subject_info_pkey
    primary key
    constraint subject_info_id_fkey
    references subject,
  description text   not null,
  room_number integer
);

alter table subject_info owner to level;

create table faculty_subject
(
  faculty_id integer not null
    constraint faculty_subject_faculty_id_fkey
    references faculty,
  subject_id integer not null
    constraint faculty_subject_subject_id_fkey
    references subject,
  constraint faculty_subject_pkey
  primary key (faculty_id, subject_id)
);

alter table faculty_subject owner to level;

create table department
(
  id         serial  not null
    constraint department_pkey
    primary key,
  name       varchar not null,
  faculty_id integer not null
    constraint department_faculty_id_fkey
    references faculty
);

alter table department owner to level;

create table student_group
(
  group_key     varchar not null
    constraint student_group_pkey
    primary key,
  department_id integer
    constraint student_group_department_id_fkey
    references department
);

alter table student_group owner to level;

create table student
(
  student_id serial not null
    constraint student_pkey
    primary key,
  first_name varchar,
  last_name  varchar,
  bith_date  date,
  group_key  varchar
    constraint student_group_key_fkey
    references student_group
);

alter table student owner to level;


