create table university
(
  id              integer not null
    constraint university_pkey
    primary key,
  name            varchar(255),
  short_name      varchar(20),
  foundation_year integer
);

alter table university
  owner to level;

create table faculty
(
  faculty_id    integer not null
    constraint faculty_pkey
    primary key,
  name          varchar(255),
  univercity_id integer
);

alter table faculty
  owner to level;

create table department
(
  id         integer not null
    constraint department_pkey
    primary key,
  name       varchar(255),
  faculty_id integer
);

alter table department
  owner to level;

create table student
(
  student_card integer not null
    constraint student_pkey
    primary key,
  last_name    varchar,
  first_name   varchar,
  bithday      date,
  group_key    varchar
);

alter table student
  owner to level;

create table subject
(
  id             integer not null
    constraint subject_pkey
    primary key,
  subject        varchar,
  count_of_hours integer,
  faculty_id     integer
);

alter table subject
  owner to level;

create table student_group
(
  group_key     varchar not null
    constraint student_group_pkey
    primary key,
  department_id integer
);

alter table student_group
  owner to level;


