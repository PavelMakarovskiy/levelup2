package ru.levelUp2.universities.jdbc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;

public class App {
    public static void main(String[] args) throws IOException, SQLException {
        UniversitiesJdbcStorage universitiesJdbcStorage = new UniversitiesJdbcStorage();
        universitiesJdbcStorage.displayUniversities();
        //3:38
        universitiesJdbcStorage.findBySql("select * from university where foundation_year > ?", 1900);
        System.out.println();
        FacultyJdbcStorage facultyJdbcStorage = new FacultyJdbcStorage();
        facultyJdbcStorage.findBySql("select * from faculty f where f.name like '%ак%'");
        //0:38

//        try (BufferedReader bufferedReader = new BufferedReader(
//            new InputStreamReader(System.in))) {
//
//            System.out.println("Enter university name: ");
//            String name = bufferedReader.readLine();
//
//            System.out.println("Enter university short_name: ");
//            String shortName = bufferedReader.readLine();
//
//            System.out.println("Enter university f_year: ");
//            int year = Integer.parseInt(bufferedReader.readLine());
//
//            universitiesJdbcStorage.createUniversity(name, shortName, year);
//        } catch (Exception exc) {
//            throw new RuntimeException(exc);
//        }

    }
}
