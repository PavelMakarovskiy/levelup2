package ru.levelUp2.universities.jdbc;

import java.sql.*;

public class UniversitiesJdbcStorage {
    private JdbcService jdbcService;

    public UniversitiesJdbcStorage() {
        jdbcService = new JdbcService();
    }

    public void createUniversity(String name, String shortName, int foundationYear) {
        if (foundationYear < 0) {
            throw new IllegalArgumentException("foundationYear must be positive");
        }

        try (Connection connection = jdbcService.openConnection()) {
            PreparedStatement statement = connection.prepareStatement(
                "insert into university(name, short_name, foundation_year) " +
                    "values (?, ?, ?)");

            statement.setString(1, name);
            statement.setString(2, shortName);
            statement.setInt(3, foundationYear);

            int rowAffected = statement.executeUpdate();
            System.out.println("Row affected: " + rowAffected);

        } catch (SQLException exc) {
            System.out.println("Could not open connection" + exc.getMessage());
            throw new RuntimeException(exc);
        }
    }

    public void displayUniversities() {
        try (Connection connection = jdbcService.openConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from university");
            while (resultSet.next()) {
                int id = resultSet.getInt(1);
                String name = resultSet.getString(2);
                String shortName = resultSet.getString(3);
                int year = resultSet.getInt(4);
                System.out.println(id + " " + name + " " + shortName + " " + year);
            }
        } catch (SQLException exc) {
            System.out.println("Could not open connection" + exc.getMessage());
            throw new RuntimeException(exc);
        }
    }

    public void findBySql(String sql, Object... args) throws SQLException {
        try (Connection connection = jdbcService.openConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            int index = 1;
            for (Object arg : args) {
                statement.setObject(index++, arg);
            }
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Integer id = rs.getInt(1);
                String name = rs.getString(2);
                String short_name = rs.getString(3);
                Integer year = rs.getInt(4);

                System.out.println(String.join("", id + name + short_name + year));
            }
        } catch (SQLException exc) {
            System.out.println("Could not open connection" + exc.getMessage());
            throw new RuntimeException(exc);
        }
    }
}
