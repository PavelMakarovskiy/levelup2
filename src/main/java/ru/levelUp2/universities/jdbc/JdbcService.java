package ru.levelUp2.universities.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JdbcService {
    public Connection openConnection() throws SQLException {
       // jdbc:postgresql://localhost:5432/level
        return DriverManager.getConnection("jdbc:postgresql://localhost:5432/level",
            "level", "level");
    }
}
